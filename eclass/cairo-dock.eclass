# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

# @ECLASS: cairo-dock.eclass
# @MAINTAINER:
# DuPol@gmx.de
#
# @CODE@
# Authors: Dustin Polke <DuPol@gmx.de>.
# @CODE@
# @BLURB: Helper eclass for cairo-dock installation.
# @DESCRIPTION:
# Provide helper functions for cairo-dock and split cairo-dock-plugins ebuilds.

inherit versionator

# @ECLASS-VARIABLE: CDP_CMakeLists_FORCE_UNPACK
# @DESCRIPTION:
# Force unpacking of CMakeLists.txt for cairo-dock-plugins if not set to "0".
: ${CDP_CMakeLists_FORCE_UNPACK:=0}

# @ECLASS-VARIABLE: CDP_P
# @INTERNAL
# @DESCRIPTION:
# Upstream's name of the plugin package
: ${CDP_P:=${PN%-*}-${PV}}
CDP_P=${CDP_P/cd/cairo-dock}

# @ECLASS-VARIABLE: CD_TYPE
# @INTERNAL
# @DESCRIPTION:
# Defines package type.
if [ "${CDP_P%-*}" == "cairo-dock-plugins" ]; then
	CD_TYPE="plug-ins"
else
	CD_TYPE="core"
fi

# @ECLASS-VARIABLE: CD_P
# @DESCRIPTION:
# Upstream's name of the package
if [ "${CD_TYPE}" == "plug-ins" ]; then
	: ${CD_P:=${CDP_P}}
else
	: ${CD_P:=${PN}-${PV}}
fi

# @ECLASS-VARIABLE: CD_PV_MAJ_MIN
# @INTERNAL
# @DESCRIPTION:
# Major and minor numbers of the version number.
: ${CD_PV_MAJ_MIN:=$(get_version_component_range 1-2)}

# @ECLASS-VARIABLE: CD_PLUGINS
# @DESCRIPTION:
# Names of all plugins to be handled by the ebuild.

SRC_URI="http://launchpad.net/cairo-dock-${CD_TYPE}/${CD_PV_MAJ_MIN}/${PV}/+download/${CD_P}.tar.gz"

S="${WORKDIR}"/${CD_P}

# @ECLASS-VARIABLE: CD_LANGS
# @DESCRIPTION:
# Languages supported for localization. linguas_ will be added to IUSE.
if [[ ! -z ${CD_LANGS} ]]; then
	for _lang in ${CD_LANGS}; do
		IUSE+=" linguas_${_lang}"
	done
	unset _lang
fi

# @FUNCTION: cairo-dock_src_unpack
# @DESCRIPTION:
# For cairo-dock, run default_src_unpack.
# For plugins, unpack only code for plugins to be build; there needs to be
# either ${P}-CMakeLists.txt or ${PN}-${CD_PV_MAJ_MIN}-CMakeLists.txt in
# ${FILESDIR}.
cairo-dock_src_unpack() {
	if [ "${CD_TYPE}" == "plug-ins" ]; then
		local target targets=( "${CDP_P}/cmake_modules" ) 
		[ ! -z "${CD_LANGS}" ] && \
			targets+=( "${CDP_P}/po" )
		for target in ${CD_PLUGINS[@]}; do
			targets+=( "${CDP_P}/${target}" )
		done
		[ "${CDP_CMakeLists_FORCE_UNPACK}" != "0" ] && \
			targets+=( "${CDP_P}/CMakeLists.txt" )
		ebegin "Unpacking parts of ${CDP_P}.tar.gz:" ${targets[@]//${CDP_P}\/}
		tar -xzf "${DISTDIR}"/${CDP_P}.tar.gz ${targets[@]}
		eend $? || die "Failed to unpack"
	else
		default_src_unpack
	fi
}

# @FUNCTION: cairo-dock_src_prepare
# @DESCRIPTION:
# For cairo-dock, apply CMakeLists.patch if it exists.
# For plugins, use ${P}-CMakeLists.txt or ${PN}-${CD_PV_MAJ_MIN}-CMakeLists.txt
# (in this order) if any exist in ${FILESDIR}; die if no CMakeLists.txt has been
# made available.
cairo-dock_src_prepare() {
	if [ "${CD_TYPE}" == "plug-ins" ]; then
		if [ "${CDP_CMakeLists_FORCE_UNPACK}" == "0" ]; then
			if [ -f "${FILESDIR}"/${P}-CMakeLists.txt ]; then
				ebegin "Using ${P}-CMakeLists.txt in '${FILESDIR}'"
				cp {"${FILESDIR}"/${P}-,"${S}"/}CMakeLists.txt
				eend $? || die
			elif [ -f "${FILESDIR}"/${PN}-${CD_PV_MAJ_MIN}-CMakeLists.txt ]; then
				ebegin "Using ${PN}-${CD_PV_MAJ_MIN}-CMakeLists.txt in '${FILESDIR}'"
				cp {"${FILESDIR}"/${PN}-${CD_PV_MAJ_MIN}-,"${S}"/}CMakeLists.txt 
				eend $? || die
				ebegin "Adjusting version to ${PV}"
				sed -e "s/@CD_VER@/${PV}/" -i "${S}"/CMakeLists.txt
				eend $? || die
			else
				die "No CMakeLists.txt made available"
			fi
		fi
	else
		if [ -f "${FILESDIR}"/${P}-CMakeLists.patch ]; then
			epatch "${FILESDIR}"/${P}-CMakeLists.patch
		elif [ -f "${FILESDIR}"/${PN}-${CD_PV_MAJ_MIN}-CMakeLists.patch ]; then
			epatch "${FILESDIR}"/${PN}-${CD_PV_MAJ_MIN}-CMakeLists.patch
		else
			ewarn "CMakeLists.txt left unpatched"
		fi
	fi

	# localization
	if [[ ! -z ${CD_LANGS} ]]; then
		if [[ -z ${LINGUAS} ]]; then
			einfo "Installing no localization"
			touch po/gentoo_linguas || die
		else
			einfo "Installing localization for the following languages:	${LINGUAS}"
			for _lang in ${LINGUAS}; do
				echo "\"${S}\"/po/${_lang}.po" >> po/gentoo_linguas || die
			done
			unset _lang
		fi
		sed -e "s/^\(file (\)GLOB \(PO_FILES\).*$/\1STRINGS gentoo_linguas \2)/" \
			-i po/CMakeLists.txt || die
	fi
}
