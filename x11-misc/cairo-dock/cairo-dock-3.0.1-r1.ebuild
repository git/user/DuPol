# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$
EAPI="5"

CD_LANGS="ar be bn ca cs cy de el eo es et eu fi fo fr gl hu id it ja ko nb nl
	oc pl pt_BR pt ro ru sk sl sr sv tr uk vi zh_CN zh_TW"

inherit cmake-utils eutils cairo-dock

DESCRIPTION="Cairo-dock is a fast, responsive, Mac OS X-like dock."
HOMEPAGE="http://www.glx-dock.org"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="crypt gtk2 xcomposite"

RDEPEND="dev-libs/dbus-glib
	dev-libs/glib:2
	dev-libs/libxml2:2
	gnome-base/librsvg:2
	media-libs/mesa
	net-misc/curl
	sys-apps/dbus
	x11-libs/cairo
	x11-libs/libXrender
	x11-libs/pango
	crypt? ( sys-libs/glibc )
	!gtk2? ( x11-libs/gtk+:3 )
	gtk2? ( x11-libs/gtk+:2 )
	xcomposite? (
		x11-libs/libXcomposite
		x11-libs/libXinerama
		x11-libs/libXtst
	)"

DEPEND="${RDEPEND}
	dev-util/intltool
	sys-devel/gettext
	virtual/pkgconfig"

WANT_CMAKE="always"

src_prepare() {
	cairo-dock_src_prepare
}

src_configure() {
	mycmakeargs+=(
		"$(cmake-utils_use_with crypt CRYPT)"
		"$(cmake-utils_use_with gtk2 GTK2)"
		"$(cmake-utils_use_with xcomposite XEXTEND)"
		"-DENABLE_GLITZ=OFF"
		"-DCMAKE_INSTALL_LIBDIR="${EPREFIX}"/usr/$(get_libdir)" )
	cmake-utils_src_configure
}

pkg_postinst() {
	elog "Additional plugins are available to extend the functionality"
	elog "of Cairo-Dock. It is recommended to install at least"
	elog "x11-misc/cairo-dock-plugins-core."
	elog
	elog "Cairo-Dock is an app that draws on a RGBA GLX visual."
	elog "Some users have noticed that if the dock is launched,"
	elog "severals qt4-based applications could crash, like skype or vlc."
	elog "If you have this problem, add the following line into your bashrc:"
	echo
	elog "alias vlc='export XLIB_SKIP_ARGB_VISUALS=1; vlc; unset XLIB_SKIP_ARGB_VISUALS'"
	elog "see http://www.qtforum.org/article/26669/qt4-mess-up-the-opengl-context.html for more details."
}
