# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $
EAPI="5"

PYTHON_COMPAT=( python2_7 )

CD_LANGS="ar cs de el es et eu fr hu id it ja nl pl pt_BR pt ru sk sr sv tr uk
	zh_CN zh_TW"

inherit cmake-utils cairo-dock python-single-r1 vala

DESCRIPTION="Official recommended plugins for cairo-dock"
HOMEPAGE="http://www.glx-dock.org"

LICENSE="|| ( LGPL-2 LGPL-2.1 LGPL-3 ) GPL-2 GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
CD_PLUGINS_IUSE="gnome xfce"
IUSE="${CD_PLUGINS_IUSE} gtk2 mono python ruby vala"

CDP_DEPEND="dev-libs/dbus-glib
	>=dev-libs/glib-2.22:2
	dev-libs/libxml2:2
	gnome-base/librsvg:2
	sys-apps/dbus
	x11-libs/cairo
	~x11-misc/cairo-dock-${PV}[gtk2=]
	!x11-misc/cairo-dock-plugins
	!x11-plugins/cairo-dock-plugins
	!gtk2? ( x11-libs/gtk+:3 )
	gtk2? ( x11-libs/gtk+:2 )"
RDEPEND="${CDP_DEPEND}
	mono? (
		dev-dotnet/glib-sharp
		dev-dotnet/ndesk-dbus
		dev-dotnet/ndesk-dbus-glib
		dev-lang/mono
	)
	python? ( ${PYTHON_DEPS} )
	ruby? ( dev-lang/ruby:* )
	vala? ( $(vala_depend)
		>=dev-libs/glib-2.26:2
	)"
DEPEND="${RDEPEND}
	dev-util/intltool
	sys-devel/gettext
	virtual/pkgconfig
	mono? ( !dev-util/pkgconf )"
REQUIRED_USE="${PYTHON_REQUIRED_USE}"

WANT_CMAKE="always"

pkg_setup() {
	CD_PLUGINS=(
		"dock-rendering"
		"desklet-rendering"
		"dialog-rendering"
		"Dbus"
		"Animated-icons"
		"icon-effect"
		"illusion"
		"drop-indicator"
	)
	use gnome && CD_PLUGINS+=( "gnome-integration" )
	use xfce && CD_PLUGINS+=( "xfce-integration" )
	use gnome || use xfce && CD_PLUGINS+=( "gvfs-integration" )
	use python && python-single-r1_pkg_setup
}

src_unpack() {
	cairo-dock_src_unpack
}

src_prepare() {
	cairo-dock_src_prepare
	if use python; then
		epatch "${FILESDIR}"/${PN}-${CD_PV_MAJ_MIN}-python_Dbus-r1.patch
		cat >> Dbus/interfaces/CMakeLists.txt <<-EOF
			add_subdirectory(bash)
			add_subdirectory(python)
			EOF
	fi
	use xfce &&	epatch \
		"${FILESDIR}"/${PN}-${CD_PV_MAJ_MIN}-xfce-integration_CMakeLists.patch
}

src_configure() {
	mycmakeargs+=(
		"$(cmake-utils_use_enable gnome GNOME-INTEGRATION)"
		"$(cmake-utils_use_enable xfce XFCE-INTEGRATION)"
		"$(cmake-utils_use_with mono MONO-INTERFACE)"
		"$(cmake-utils_use_with python PYTHON-INTERFACE)"
		"$(cmake-utils_use_with ruby RUBY-INTERFACE)"
		"$(cmake-utils_use_with vala VALA-INTERFACE)"
	)
	cmake-utils_src_configure
}

src_compile() {
	cmake-utils_src_compile

	if use python; then
		einfo "Create python script for bash"
		cd "${BUILD_DIR}"/Dbus/interfaces/bash || die "Failed to change to '${BUILD_DIR}/Dbus/interfaces/bash'"
		"${PYTHON}" setup.py -q build || die
		einfo "Create python script for python"
		cd "${BUILD_DIR}"/Dbus/interfaces/python || die "Failed to change to '${BUILD_DIR}/Dbus/interfaces/python'"
		"${PYTHON}" setup.py -q build || die
	fi
}

src_install() {
	cmake-utils_src_install

	if use python; then
		einfo "Install python scripts"
		insinto "$(python_get_sitedir)"/cairodock
		doins "${BUILD_DIR}"/Dbus/interfaces/bash/build/lib/*.py
		doins "${BUILD_DIR}"/Dbus/interfaces/python/build/lib/*.py
		python_fix_shebang "${ED}/$(python_get_sitedir)"/cairodock/*.py
		touch "${ED}/$(python_get_sitedir)"/cairodock/__init__.py
		python_optimize
	fi
}
