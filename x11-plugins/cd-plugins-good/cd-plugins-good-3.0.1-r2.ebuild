# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $
EAPI="5"

inherit cmake-utils cairo-dock

DESCRIPTION="Official plugins for cairo-dock with minor flaws."
HOMEPAGE="http://www.glx-dock.org"

LICENSE="|| ( LGPL-2 LGPL-2.1 LGPL-3 ) GPL-2 GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
CD_PLUGIN_IUSE="compiz dnd2share folders mail musicplayer pulseaudio
	recent-events remote-control rssreader stack status-notifier system-monitor
	systray tomboy xklavier"
IUSE="${CD_PLUGIN_IUSE} fftw gtk2 lm_sensors"
REQUIRED_USE="|| ( ${CD_PLUGIN_IUSE} )
	fftw? ( pulseaudio )
	lm_sensors? ( system-monitor )"
CDP_DEPEND="dev-libs/dbus-glib
	>=dev-libs/glib-2.22:2
	dev-libs/libxml2:2
	gnome-base/librsvg:2
	sys-apps/dbus
	x11-libs/cairo
	~x11-misc/cairo-dock-${PV}[gtk2=]
	!x11-misc/cairo-dock-plugins
	!x11-plugins/cairo-dock-plugins
	!gtk2? ( x11-libs/gtk+:3 )
	gtk2? ( x11-libs/gtk+:2 )"
RDEPEND="${CDP_DEPEND}
	lm_sensors? ( sys-apps/lm_sensors )
	mail? ( net-libs/libetpan )
	pulseaudio? ( media-sound/pulseaudio )
	fftw? ( sci-libs/fftw:3.0 )
	recent-events? ( dev-libs/libzeitgeist )
	status-notifier? (
		!gtk2? ( dev-libs/libdbusmenu[gtk3] )
		gtk2? ( dev-libs/libdbusmenu[gtk] )
	)
	xklavier? ( x11-libs/libxklavier )"
DEPEND="${RDEPEND}
	dev-util/intltool
	sys-devel/gettext
	virtual/pkgconfig"

WANT_CMAKE="always"

pkg_setup() {
	use compiz && CD_PLUGINS+=( "Composite-Manager" )
	use dnd2share && CD_PLUGINS+=( "dnd2share" )
	use folders && CD_PLUGINS+=( "Folders ")
	use mail && CD_PLUGINS+=( "mail" )
	use musicplayer && CD_PLUGINS+=( "musicPlayer" )
	use pulseaudio && CD_PLUGINS+=( "Impulse" )
	use recent-events && CD_PLUGINS+=( "Recent-Events" )
	use remote-control && CD_PLUGINS+=( "Remote-Control" )
	use rssreader && CD_PLUGINS+=( "RSSreader" )
	use stack && CD_PLUGINS+=( "stack" )
	use status-notifier && CD_PLUGINS+=( "Status-Notifier" )
	use system-monitor && CD_PLUGINS+=( "System-Monitor" )
	use systray && CD_PLUGINS+=( "systray" )
	use tomboy && CD_PLUGINS+=( "tomboy" )
	use xklavier && CD_PLUGINS+=( "keyboard-indicator" )
}

src_unpack() {
	cairo-dock_src_unpack
}

src_prepare() {
	cairo-dock_src_prepare
}

src_configure() {
	mycmakeargs+=(
		"$(cmake-utils_use_enable compiz COMPOSITE-MANAGER-PLUGIN)"
		"$(cmake-utils_use_enable dnd2share DND2SHARE-PLUGIN)"
		"$(cmake-utils_use_enable folders FOLDERS-PLUGIN)"
		"$(cmake-utils_use_enable mail MAIL-PLUGIN)"
		"$(cmake-utils_use_enable musicplayer MUSICPLAYER-PLUGIN)"
		"$(cmake-utils_use_enable pulseaudio IMPULSE-PLUGIN)"
		"$(cmake-utils_use_with fftw FFTW3-SUPPORT)"
		"$(cmake-utils_use_enable recent-events RECENT-EVENTS-PLUGIN)"
		"$(cmake-utils_use_enable remote-control REMOTE-CONTROL-PLUGIN)"
		"$(cmake-utils_use_enable rssreader RSSREADER-PLUGIN)"
		"$(cmake-utils_use_enable stack STACK-PLUGIN)"
		"$(cmake-utils_use_enable status-notifier STATUS-NOTIFIER-PLUGIN)"
		"$(cmake-utils_use_enable system-monitor SYSTEM-MONITOR-PLUGIN)"
		"$(cmake-utils_use_with lm_sensors SENSORS-SUPPORT)"
		"$(cmake-utils_use_enable systray SYSTRAY-PLUGIN)"
		"$(cmake-utils_use_enable tomboy TOMBOY-PLUGIN)"
		"$(cmake-utils_use_enable xklavier KEYBOARD-INDICATOR-PLUGIN)"
		)
	cmake-utils_src_configure
}
